//192.168.0.25:8888
use std::net::{TcpListener, TcpStream};
use std::io::{BufReader, BufWriter, Write, Read, BufRead};
use std::time::Duration;
use std::mem::transmute;
use std::string::ToString;
use std::{thread, time};
use std::sync::Arc;
use std::mem;
use std::io;

pub mod SBTCP;
pub mod RECEIVE;
pub mod functions;

pub mod soupbin_session;
use soupbin_session::soupbin_session as SOUP;

fn main()
{
    // start soupbin session
    let session1 = SOUP::new();
    session1.start();

    // WRITE(SEND) IS DONE AT SOUPBIN_SESSION
}
