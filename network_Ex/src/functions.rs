use std::net::{TcpListener, TcpStream};
use std::io::{BufReader, BufWriter, Write, Read, BufRead};
use std::time::Duration;
use std::mem::transmute;
use std::string::ToString;
use std::{thread, time};
use std::sync::Arc;
use std::mem;
use std::io;
use std::str;

pub fn CFS(rv: &Vec<u8>, i: usize, j: usize) -> [u8; 12]
{
    let mut m:[u8; 12] = [0; 12]; 
    m.copy_from_slice(&rv[i..j]);
    
    m
}

pub fn from_u8_to_u64(rv: &Vec<u8>, i: usize) -> u64
{
    let n:u64 = unsafe
    {
        mem::transmute_copy::<(u8, u8, u8, u8, u8, u8, u8, u8), u64>
        (&(rv[i],rv[i+1],rv[i+2],rv[i+3],
        rv[i+4],rv[i+5],rv[i+6],rv[i+7]))
    };
    
    n
}

pub fn from_u8_to_u32(rv: &Vec<u8>, i: usize) -> u32
{
    let n:u32 = unsafe
    {
        mem::transmute_copy::<(u8, u8, u8, u8), u32>
        (&(rv[i],rv[i+1],rv[i+2],rv[i+3]))
    };
    
    n
}

pub fn from_u8_to_i32(rv: &Vec<u8>, i: usize) -> i32
{
    let n:i32 = unsafe
    {
        mem::transmute_copy::<(u8, u8, u8, u8), i32>
        (&(rv[i],rv[i+1],rv[i+2],rv[i+3]))
    };
    
    n
}

pub fn from_u8_to_string(rv: &Vec<u8>, i: usize, j: usize) -> String
{
    let mut s = match str::from_utf8(&rv[i..j])
    {
        Ok(v) => v, Err(e) => panic!("Invalid UTF-8 sequence: {}", e)
    };
    
    String::from(s)
}
