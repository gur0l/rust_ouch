use std::net::{Shutdown, TcpListener, TcpStream};
use std::io::{BufReader, BufWriter, Write, Read, BufRead};
use std::time::Duration;
use std::mem::transmute;
use std::string::ToString;
use std::{thread, time};
use std::sync::Arc;
use std::mem;
use std::io;
use std::str;

use SBTCP::login_request_packet::login_request_packet as SBTCP_login_request;
use SBTCP::unsequenced_data_packet::unsequenced_data_packet as SBTCP_unsequenced_data;
use SBTCP::logout_request_packet::logout_request_packet as SBTCP_logout_request;

use SBTCP::OUCH::enter_order::enter_order as OUCH_enter_order;
use SBTCP::OUCH::replace_order::replace_order as OUCH_replace_order;
use SBTCP::OUCH::cancel_order::cancel_order as OUCH_cancel_order;
use SBTCP::OUCH::cancel_by_order_id::cancel_by_order_id as OUCH_cancel_by_order_id;

use RECEIVE::debug_packet::debug_packet as RECEIVE_debug;
use RECEIVE::login_accepted_packet::login_accepted_packet as RECEIVE_login_accepted;
use RECEIVE::login_rejected_packet::login_rejected_packet as RECEIVE_login_rejected;
use RECEIVE::sequenced_data_packet::sequenced_data_packet as RECEIVE_sequenced_data;

pub struct soupbin_session
{
    socket: Option<TcpStream>
}

pub fn send_heartbeat(mut s: &TcpStream)
{
    println!("Heartbeat thread started.");

    let HB: [u8; 3] = [0, 1, 82u8];
    let one_second = time::Duration::from_millis(1000);
    let now = time::Instant::now();
    
    loop
    {
        thread::sleep(one_second);
        s.write(&HB);
        println!("Heartbeat sent.");
    }
}

pub fn handle_reader(mut s: &TcpStream)
{
    println!("Read thread started");
    
    loop
    {
        let mut packet_length_buffer: [u8; 2] = [0,0];
        println!("packetLengthBuffer: {:?}", packet_length_buffer); // check
        let Error2 = s.read(&mut packet_length_buffer);
        
        // convert received length to integer
        let packet_length: u16 = unsafe
        { 
            mem::transmute_copy::<(u8, u8), u16>
            (&(packet_length_buffer[0],packet_length_buffer[1])
        )};
        
        // create vector with the converted length
        let mut received_message:Vec<u8> = vec![0; packet_length as usize];
        
        // read incoming packet
        let Error3 = s.read(&mut received_message);
        
        // authorize packet
        let packet_type = received_message[0] as char;
        println!("Received Packet Type: {}", packet_type);
        
        if packet_type == '+'
        {
            let debug_message = RECEIVE_debug::new(&received_message);
        }
        else if packet_type == 'J'
        {
            let login_rejected_message = RECEIVE_login_rejected::new(&received_message);
        }
        else if packet_type == 'S'
        {
            let sequenced_data_message = RECEIVE_sequenced_data::new(received_message);
        }
        else if packet_type == 'H'
        {
            println!("HEARTBEAT RECEIVED!");
        }
        else if packet_type == 'Z'
        {
            println!("SHUTDOWN RECEIVED!");
            s.shutdown(Shutdown::Both).expect("shutdown call failed");
            break;
        }
    }
}

pub fn create_writer(mut s: &TcpStream)
{
    println!("Writer thread started");
    
    //loop
    //{
    // certification order not implemented yet, but it's ez, end of internship.
    let logout_request = SBTCP_logout_request::new();

    let unseq_packet = SBTCP_unsequenced_data::new(logout_request.serialize());
    s.write(&unseq_packet.ready_buff);
    //}
}


impl soupbin_session
{
    pub fn new() -> soupbin_session
    {
        soupbin_session
        {
            socket: None
        }
    }
    
    pub fn start(mut self)
    {
        // establish TCP connection 
        self.socket = Some(TcpStream::connect("127.0.0.1:1234").
        expect("Couldn't connect to the server..."));
        
        let mut socket2:TcpStream = self.socket.unwrap();
        
        // create login request packet
        let login_length:u16 = 49;
        let length_to_bytes: [u8; 2] = unsafe { transmute(login_length.to_be()) };
        
        // object created here...
        let login = SBTCP_login_request::new
        (
            String::from("osman"),
            String::from("asd"),
            String::from("aaaaaaaaaa"),
            String::from("1")
        );
        
        // ...then serialized
        let packet_to_send: [u8; 49] = login.serialize();
        
        // send packet length
        let Error1 = socket2.write(&length_to_bytes);
        println!("Sent login packet size: {:?}",Error1);
        
        // send packet itself
        let Error2 = socket2.write(&packet_to_send);
        println!("Sent packet: {:?}",Error2);
        
        // create base to receive length
        let mut packet_length_buffer: [u8; 2] = [0,0];
        let Error2 = socket2.read(&mut packet_length_buffer);
        println!("packetLengthBuffer: {:?}", packet_length_buffer); // check
        
        // convert received length to integer
        let packet_length: u16 = unsafe
        { 
            mem::transmute_copy::<(u8, u8), u16>
            (&(packet_length_buffer[0],packet_length_buffer[1])
        )};
        
        // create vector with the converted length
        let mut received_message:Vec<u8> = vec![0; packet_length as usize];
        
        // read incoming packet
        let Error3 = socket2.read(&mut received_message);
        
        // authorize packet
        let packet_type = received_message[0] as char;
        println!("Received Packet Type: {}", packet_type);
        
        if packet_type == 'A'
        {
            let login_accepted_message = RECEIVE_login_accepted::new(&received_message);
            
            //ARC
            let mut arched_stream = Arc::new(socket2);
            let mut val1 = Arc::clone(&arched_stream);
            let mut val2 = Arc::clone(&arched_stream);
            let mut val3 = Arc::clone(&arched_stream);
            
            //HB
            let handle1 = thread::spawn(move || 
            {
                send_heartbeat(&val1);
            });
            
            //READ
            let handle2 = thread::spawn(move || 
            {
                handle_reader(&val2);
            });
            
            //WRITE
            let create1 = thread::spawn(move || 
            {
                create_writer(&val3);
            });
        }
        else
        {
            panic!("DIE");
        }
    }
    
    /*pub fn send_unsequenced_data_packet(mut self, unseq_packet: &SBTCP_unsequenced_data)
    {
        let mut socket2:&TcpStream = &self.socket.unwrap();
        
        socket2.write(&unseq_packet.ready_buff);
    }*/
    
    /*pub fn stop()
    {
        self.socket.shutdown(Shutdown::Both).expect("shutdown call failed");
    }*/
}
