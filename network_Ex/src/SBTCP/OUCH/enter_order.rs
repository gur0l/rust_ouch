use std::str;
use std::mem::transmute;

pub struct enter_order
{
    order_token: String,
    order_book_ID: u32,
    side: char,
    quantity: u64,
    price: i32,
    time_in_force: u8,
    open_close: u8,
    client_account: String,
    customer_info: String,
    exchange_info: String,
    display_quantity: u64,
    client_category: u8,
    reserved: [u8; 8]
}

pub fn add_space_to_end(toBeConverted_string: &str, desired_len: usize) -> String
{
    let mut tmp = String::from(toBeConverted_string);

    for i in 0..desired_len-tmp.len()
    {
        if tmp.len() < desired_len
        {
            tmp.push_str(" ");
        }
    }
    
    tmp
}

impl enter_order
{
    pub fn new
    (
        ot: String,
        obi: u32,
        s: char,
        q: u64,
        p: i32,
        tif: u8,
        oc: u8,
        ca: String,
        ci: String,
        ei: String,
        dq: u64,
        cc: u8
    ) -> enter_order
    {
        enter_order
        {
            order_token: ot,
            order_book_ID: obi,
            side: s,
            quantity: q,
            price: p,
            time_in_force: tif,
            open_close: oc,
            client_account: ca,
            customer_info: ci,
            exchange_info: ei,
            display_quantity: dq,
            client_category: cc,
            reserved: [0; 8]
        }
    }
    
    pub fn serialize(mut self) -> Vec<u8>
    {
        let mut serialized:[u8; 114] = [0; 114];
        
        //message type
        serialized[0] = 79u8;
        
        //order token
        let mut returned_token = add_space_to_end(&self.order_token, 14);
        serialized[1..15].copy_from_slice(returned_token.as_bytes());
        
        //order book id
        let mut bytes4: [u8; 4] = unsafe { transmute((self.order_book_ID).to_be()) };
        serialized[15..19].copy_from_slice(&bytes4);
        
        //side
        serialized[19] = 66u8;
        
        //quantity
        let mut bytes8: [u8; 8] = unsafe { transmute((self.quantity).to_be()) };
        serialized[20..28].copy_from_slice(&bytes8);
        
        //price
        bytes4 = unsafe { transmute((self.price).to_be()) };
        serialized[28..32].copy_from_slice(&bytes4);
        
        //time in force
        serialized[32] = self.time_in_force;
        
        //open close
        serialized[33] = self.open_close;
        
        //client/account
        returned_token = add_space_to_end(&self.client_account, 16);
        serialized[34..50].copy_from_slice(returned_token.as_bytes());
        
        //customer info
        returned_token = add_space_to_end(&self.customer_info, 15);
        serialized[50..65].copy_from_slice(returned_token.as_bytes());
        
        //exchange info
        returned_token = add_space_to_end(&self.exchange_info, 16);
        serialized[65..81].copy_from_slice(returned_token.as_bytes());
        
        //display quantity
        bytes8 = unsafe { transmute((self.display_quantity).to_be()) };
        serialized[97..105].copy_from_slice(&bytes8);
        
        //client category
        serialized[105] = self.client_category;
        
        //nothing to the reserved, return.
        let x = serialized.to_vec();
        x
    }
}
