use std::str;
use std::mem::transmute;

pub struct cancel_by_order_id
{
    order_book_ID: u32,
    side: char,
    order_ID: u64
}

impl cancel_by_order_id
{
    pub fn new(obi: u32, s: char, oi: u64) -> cancel_by_order_id
    {
        cancel_by_order_id
        {
            order_book_ID: obi,
            side: s,
            order_ID: oi
        }
    }
    
    pub fn serialize(mut self) -> Vec<u8>
    {
        let mut serialized:[u8; 14] = [0; 14];
        
        //message type
        serialized[0] = 89u8;
        
        //order book id
        let mut bytes4: [u8; 4] = unsafe { transmute((self.order_book_ID).to_be()) };
        serialized[1..5].copy_from_slice(&bytes4);
        
        //side
        serialized[5] = 66u8;
        
        //order ID
        let mut bytes8: [u8; 8] = unsafe { transmute((self.order_ID).to_be()) };
        serialized[6..14].copy_from_slice(&bytes8);
        
        let x = serialized.to_vec();
        x
    }
}
