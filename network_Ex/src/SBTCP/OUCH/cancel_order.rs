use std::str;
use std::mem::transmute;

pub struct cancel_order
{
    order_token: String
}

pub fn add_space_to_end(toBeConverted_string: &str, desired_len: usize) -> String
{
    let mut tmp = String::from(toBeConverted_string);

    for i in 0..desired_len-tmp.len()
    {
        if tmp.len() < desired_len
        {
            tmp.push_str(" ");
        }
    }
    
    tmp
}

impl cancel_order
{
    pub fn new(ot: String) -> cancel_order
    {
        cancel_order
        {
            order_token: ot
        }
    }
    
    pub fn serialize(mut self) -> Vec<u8>
    {
        let mut serialized:[u8; 15] = [0; 15];
        
        //message type
        serialized[0] = 88u8;

        //order token
        let mut returned_token = add_space_to_end(&self.order_token, 14);
        serialized[1..15].copy_from_slice(returned_token.as_bytes());

        let x = serialized.to_vec();
        x
    }
}
