use std::str;
use std::mem::transmute;

pub struct replace_order
{
    existing_order_token: String,
    replacement_order_token: String,
    quantity: u64,
    price: i32,
    open_close: u8,
    client_account: String,
    customer_info: String,
    exchange_info: String,
    display_quantity: u64,
    client_category: u8,
    reserved: [u8; 8]
}

pub fn add_space_to_end(toBeConverted_string: &str, desired_len: usize) -> String
{
    let mut tmp = String::from(toBeConverted_string);

    for i in 0..desired_len-tmp.len()
    {
        if tmp.len() < desired_len
        {
            tmp.push_str(" ");
        }
    }
    
    tmp
}

impl replace_order
{
    pub fn new
    (
        eot: String,
        rot: String,
        q: u64,
        p: i32,
        oc: u8,
        ca: String,
        ci: String,
        ei: String,
        dq: u64,
        cc: u8
    ) -> replace_order
    {
        replace_order
        {
            existing_order_token: eot,
            replacement_order_token: rot,
            quantity: q,
            price: p,
            open_close: oc,
            client_account: ca,
            customer_info: ci,
            exchange_info: ei,
            display_quantity: dq,
            client_category: cc,
            reserved: [0; 8]
        }
    }
    
    pub fn serialize(mut self) -> Vec<u8>
    {
        let mut serialized:[u8; 122] = [0; 122];
        
        //message type
        serialized[0] = 85u8;
        
        //existing order token
        let mut returned_token = add_space_to_end(&self.existing_order_token, 14);
        serialized[1..15].copy_from_slice(returned_token.as_bytes());
        
        //replacement order token
        returned_token = add_space_to_end(&self.replacement_order_token, 14);
        serialized[15..29].copy_from_slice(returned_token.as_bytes());
        
        //quantity
        let mut bytes8: [u8; 8] = unsafe { transmute((self.quantity).to_be()) };
        serialized[29..37].copy_from_slice(&bytes8);
        
        //price
        let mut bytes4: [u8; 4] = unsafe { transmute((self.price).to_be()) };
        serialized[37..41].copy_from_slice(&bytes4);
        
        //open close
        serialized[41] = self.open_close;
        
        //client/account
        returned_token = add_space_to_end(&self.client_account, 16);
        serialized[42..58].copy_from_slice(returned_token.as_bytes());
        
        //customer info
        returned_token = add_space_to_end(&self.customer_info, 15);
        serialized[58..73].copy_from_slice(returned_token.as_bytes());
        
        //exchange info
        returned_token = add_space_to_end(&self.exchange_info, 16);
        serialized[73..89].copy_from_slice(returned_token.as_bytes());
        
        //display quantity
        bytes8 = unsafe { transmute((self.display_quantity).to_be()) };
        serialized[105..113].copy_from_slice(&bytes8);
        
        //client category
        serialized[113] = self.client_category;
        
        //nothing to the reserved, return.
        let x = serialized.to_vec();
        x
    }
}
