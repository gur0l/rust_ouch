use std::str;
use std::mem::transmute;

pub struct unsequenced_data_packet
{
    pub ready_buff: Vec<u8>
}

pub fn OUCH_to_unsequenced_data(rv: Vec<u8>) -> Vec<u8>
{
    let to_unseq_length:u16 = (rv.len()+3) as u16;

    let mut bytes4: [u8; 2] = unsafe
    {
        transmute(to_unseq_length.to_be())
    };
    
    let mut ready_unseq:Vec<u8> = vec![0; to_unseq_length as usize];
    
    ready_unseq[0..2].copy_from_slice(&bytes4);
    ready_unseq[2] = 85u8;
    ready_unseq[3..117].copy_from_slice(&rv);
    
    ready_unseq
}

impl unsequenced_data_packet
{
    pub fn new(received_vector: Vec<u8>) -> unsequenced_data_packet
    {
        unsequenced_data_packet
        {
            ready_buff: OUCH_to_unsequenced_data(received_vector)
        }
    }
}
