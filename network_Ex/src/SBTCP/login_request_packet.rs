use std::mem::transmute;

pub struct login_request_packet
{
    username: String,
    password: String,
    requested_session: String,
    requested_sequence_number: String,
}

pub fn add_space_to_beginning(toBeConverted_string: &str, desired_len: usize) -> String
{
    let mut s = String::from(" ");
    let mut tmp = String::from(toBeConverted_string);

    for i in 0..desired_len-toBeConverted_string.len()
    {
        if toBeConverted_string.len() < desired_len
        {
            s.push_str(&tmp);
            tmp = s;
            s = String::from(" ");
        }
    }
    
    tmp
}

impl login_request_packet
{
    pub fn new(x: String, y: String, k: String, l: String) -> login_request_packet
    {
        login_request_packet
        {
            username: x,
            password: y,
            requested_session: k,
            requested_sequence_number: l
        }
    }
    
    pub fn serialize(self) -> [u8; 49]
    {
        let returned_username = add_space_to_beginning(&self.username, 6);
        let returned_password = add_space_to_beginning(&self.password, 10);
        let returned_requested_sequence_number = add_space_to_beginning(&self.requested_sequence_number, 20);
        
        let mut serialized:[u8; 49] = [0; 49];
        
        let len:u16 = 47;

        let bytes: [u8; 2] = unsafe { transmute(len.to_be()) };
        
        serialized[0..2].copy_from_slice(&bytes); //problem 1
        serialized[2] = 76u8;
        serialized[3..9].copy_from_slice(returned_username.as_bytes());
        serialized[9..19].copy_from_slice(returned_password.as_bytes());
        serialized[19..29].copy_from_slice((self.requested_session).as_bytes());
        serialized[29..49].copy_from_slice((returned_requested_sequence_number).as_bytes());
        
        serialized
    }
}
