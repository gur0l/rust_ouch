pub struct logout_request_packet{}

impl logout_request_packet
{
    pub fn new() -> logout_request_packet { logout_request_packet{} }
    
    pub fn serialize(self) -> Vec<u8> { let SD:Vec<u8> = vec![0, 1, 79u8]; SD }
}
