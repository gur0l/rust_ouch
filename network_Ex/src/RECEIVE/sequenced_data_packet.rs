use std::str;
use std::mem::transmute;

use RECEIVE::OUCH::order_accepted::order_accepted as OUCH_order_accepted;
use RECEIVE::OUCH::order_rejected::order_rejected as OUCH_order_rejected;
use RECEIVE::OUCH::order_replaced::order_replaced as OUCH_order_replaced;
use RECEIVE::OUCH::order_canceled::order_canceled as OUCH_order_canceled;
use RECEIVE::OUCH::order_executed::order_executed as OUCH_order_executed;

pub struct sequenced_data_packet
{
    packet_type: char
}

pub fn to_OUCH(rv : &Vec<u8>) -> char
{
    let messageType = rv[0] as char; //as char;
    
    if messageType == 'A'
    {
        let order_accepted_message = OUCH_order_accepted::new(&rv);
    }
    else if messageType == 'J'
    {
        let order_rejected_message = OUCH_order_rejected::new(&rv);
    }
    else if messageType == 'U'
    {
        let order_replaced_message = OUCH_order_replaced::new(&rv);
    }
    else if messageType == 'C'
    {
        let order_canceled_message = OUCH_order_canceled::new(&rv);
    }
    else if messageType == 'E'
    {
        let order_executed_message = OUCH_order_executed::new(&rv);
    }
    
    messageType
}

impl sequenced_data_packet
{
    pub fn new(received_vector: Vec<u8>) -> sequenced_data_packet
    {
        sequenced_data_packet
        {
            packet_type: to_OUCH(&received_vector)
        }
    }
}
