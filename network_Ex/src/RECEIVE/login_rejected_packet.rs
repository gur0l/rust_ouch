use functions;

pub struct login_rejected_packet
{
    reject_reason_code: char
}

impl login_rejected_packet
{
    pub fn new(received_vector: &Vec<u8>) -> login_rejected_packet
    {
        login_rejected_packet
        {
            reject_reason_code: received_vector[3] as char
        }
    }
}
