use functions;

pub struct debug_packet
{
    text: String
}

impl debug_packet
{
    pub fn new(received_vector: &Vec<u8>) -> debug_packet
    {
        debug_packet
        {
            text: functions::from_u8_to_string(&received_vector, 9, received_vector.len())
        }
    }
}
