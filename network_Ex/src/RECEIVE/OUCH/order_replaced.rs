use functions;

pub struct order_replaced
{
    timestamp: u64,
    replacement_order_token: String,
    previous_order_token: String,
    order_book_ID: u32,
    side: char,
    order_ID: u64,
    quantity: u64,
    price: i32,
    time_in_force: u8,
    open_close: u8,
    client_account: String,
    order_state: u8,
    customer_info: String,
    exchange_info: String,
    pre_trade_quantity: u64,
    display_quantity: u64,
    client_category: u8
}

impl order_replaced
{
    pub fn new(received_vector: &Vec<u8>) -> order_replaced
    {
        order_replaced
        {
            timestamp: functions::from_u8_to_u64(&received_vector, 1),
            replacement_order_token: functions::from_u8_to_string(&received_vector, 9, 23),
            previous_order_token: functions::from_u8_to_string(&received_vector, 23, 37),
            order_book_ID: functions::from_u8_to_u32(&received_vector, 37),
            side: received_vector[41] as char,
            order_ID: functions::from_u8_to_u64(&received_vector, 42),
            quantity: functions::from_u8_to_u64(&received_vector, 50),
            price: functions::from_u8_to_i32(&received_vector, 58),
            time_in_force: received_vector[62],
            open_close: received_vector[63],
            client_account: functions::from_u8_to_string(&received_vector, 64, 80),
            order_state: received_vector[80],
            customer_info: functions::from_u8_to_string(&received_vector, 81, 96),
            exchange_info: functions::from_u8_to_string(&received_vector, 96, 112),
            pre_trade_quantity: functions::from_u8_to_u64(&received_vector, 128),
            display_quantity: functions::from_u8_to_u64(&received_vector, 136),
            client_category: received_vector[144]
        }
    }
}
