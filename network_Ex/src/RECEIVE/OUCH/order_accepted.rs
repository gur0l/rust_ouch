use functions;

pub struct order_accepted
{
    timestamp: u64,
    order_token: String,
    order_book_ID: u32,
    side: char,
    order_ID: u64,
    quantity: u64,
    price: i32,
    time_in_force: u8,
    open_close: u8,
    client_account: String,
    order_state: u8,
    customer_info: String,
    exchange_info: String,
    pre_trade_quantity: u64,
    display_quantity: u64,
    client_category: u8
}

impl order_accepted
{
    pub fn new(received_vector: &Vec<u8>) -> order_accepted
    {
        order_accepted
        {
            timestamp: functions::from_u8_to_u64(&received_vector, 1),
            order_token: functions::from_u8_to_string(&received_vector, 9, 23),
            order_book_ID: functions::from_u8_to_u32(&received_vector, 23),
            side: received_vector[27] as char,
            order_ID: functions::from_u8_to_u64(&received_vector, 28),
            quantity: functions::from_u8_to_u64(&received_vector, 36),
            price: functions::from_u8_to_i32(&received_vector, 44),
            time_in_force: received_vector[48],
            open_close: received_vector[49],
            client_account: functions::from_u8_to_string(&received_vector, 50, 66),
            order_state: received_vector[66],
            customer_info: functions::from_u8_to_string(&received_vector, 67, 82),
            exchange_info: functions::from_u8_to_string(&received_vector, 82, 98),
            pre_trade_quantity: functions::from_u8_to_u64(&received_vector, 114),
            display_quantity: functions::from_u8_to_u64(&received_vector, 122),
            client_category: received_vector[130]
        }
    }
}
