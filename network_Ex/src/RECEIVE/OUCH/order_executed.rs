use functions;

pub struct order_executed
{
    timestamp: u64,
    order_token: String,
    order_book_ID: u32,
    traded_quantity: u64,
    trade_price: i32,
    match_ID: [u8; 12],
    client_category: u8,
    reserved: [u8; 16]
}

impl order_executed
{
    pub fn new(received_vector: &Vec<u8>) -> order_executed
    {
        order_executed
        {
            timestamp: functions::from_u8_to_u64(&received_vector, 1),
            order_token: functions::from_u8_to_string(&received_vector, 9, 23),
            order_book_ID: functions::from_u8_to_u32(&received_vector, 23),
            traded_quantity: functions::from_u8_to_u64(&received_vector, 27),
            trade_price: functions::from_u8_to_i32(&received_vector, 35),
            match_ID: functions::CFS(&received_vector, 39, 51),
            client_category: received_vector[51],
            reserved: [0; 16],
        }
    }
}

