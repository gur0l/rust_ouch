use functions;

pub struct order_canceled
{
    timestamp: u64,
    order_token: String,
    order_book_ID: u32,
    side: char,
    order_ID: u64,
    reason: u8
}

impl order_canceled
{
    pub fn new(received_vector: &Vec<u8>) -> order_canceled
    {
        order_canceled
        {
            timestamp: functions::from_u8_to_u64(&received_vector, 1),
            order_token: functions::from_u8_to_string(&received_vector, 9, 23),
            order_book_ID: functions::from_u8_to_u32(&received_vector, 23),
            side: received_vector[27] as char,
            order_ID: functions::from_u8_to_u64(&received_vector, 28),
            reason: received_vector[36]
        }
    }
}
