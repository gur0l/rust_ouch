use functions;

pub struct order_rejected
{
    timestamp: u64,
    order_token: String,
    reject_code: i32
}

impl order_rejected
{
    pub fn new(received_vector: &Vec<u8>) -> order_rejected
    {
        order_rejected
        {
            timestamp: functions::from_u8_to_u64(&received_vector, 1),
            order_token: functions::from_u8_to_string(&received_vector, 9, 23),
            reject_code: functions::from_u8_to_i32(&received_vector, 23)
        }
    }
}
