use functions;

pub struct login_accepted_packet
{
    session: String,
    sequence_number: String
}

impl login_accepted_packet
{
    pub fn new(received_vector: &Vec<u8>) -> login_accepted_packet
    {
        login_accepted_packet
        {
            session: functions::from_u8_to_string(&received_vector, 3, 13),
            sequence_number: functions::from_u8_to_string(&received_vector, 13, 43)
        }
    }
}
