use std::net::{TcpListener, TcpStream};
use std::io::{BufReader, BufWriter, Write, Read, BufRead};
use std::string::ToString;
use std::sync::Arc;
use std::thread;
use std::mem;
use std::io;

fn handle_reader(mut s: &TcpStream)
{
    let mut packetLengthBuffer: [u8; 2] = [0,0]; // converted to array.
    let Error1 = s.read(&mut packetLengthBuffer); // message 
                
    println!("packetLengthBuffer: {:?}", packetLengthBuffer);
}

fn handle_writer(mut s: &TcpStream)
{
    let mut send_Buffer: [u8; 2] = [11,12]; // array.
    let Error2 = s.write(&send_Buffer); // message   
    
    println!("Sent");
}

fn main()
{
    let mut stream = Arc::new(TcpStream::connect("127.0.0.1:10035").
    expect("Couldn't connect to the server..."));
    
    let mut val = Arc::clone(&stream);
    
    let handle1 = thread::spawn(move || 
    {
        handle_reader(&val);
    });
    
    handle_writer(&stream);
    
    handle1.join();
}
