use std::str;
use std::mem::transmute;

pub struct tested
{
    name: String
}

/*pub fn to_name(rv : &Vec<u8>) -> String
{
    let mut s = match str::from_utf8(&rv[0..12])
    {
        Ok(v) => v,
        Err(e) => panic!("Invalid UTF-8 sequence: {}", e)
    };
    String::from(s)
}*/

impl tested
{
    pub fn new(received_vector: &Vec<u8>) -> tested
    {
        tested
        {
            name: tested::to_name(&received_vector),
        }
    }
    
    pub fn to_name(rv : &Vec<u8>) -> String
    {
        let mut s = match str::from_utf8(&rv[0..12])
        {
            Ok(v) => v,
            Err(e) => panic!("Invalid UTF-8 sequence: {}", e)
        };
        String::from(s)
    }
}

fn main()
{
    let mut name = String::from("Mehmet");
    name.push_str(" Gurol");
    
    let bytes = name.into_bytes();
    
    let test1 = tested::new(&bytes);
    
    println!("Test: {}", test1.name);
}
