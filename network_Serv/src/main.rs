use std::net::{TcpListener, TcpStream};
use std::io::{BufReader, BufWriter, Write, BufRead};
use std::mem::transmute;

fn handle_client(stream: TcpStream)
{
    println!("Client connected");

    let to_be_sent_length:u16 = 6;
    let to_be_sent_bytes: [u8; 2] = unsafe { transmute(to_be_sent_length.to_be()) };

    let mut writer = BufWriter::new(&stream);
    println!("to_be_sent_bytes {:?}", to_be_sent_bytes);
    writer.write(&to_be_sent_bytes).expect("could not write");
    writer.flush().expect("could not flush");
    
    /*
    TOMORROW GIVE IT TO ME!
    */
    
    writer.write("SABC".as_bytes()).expect("could not write");
    writer.flush().expect("could not flush");

    let mut reader = BufReader::new(&stream);
    let mut response = String::new();
    reader.read_line(&mut response).expect("could not read");
    println!("Server received {}", response);
    println!("");
}

fn main()
{
    let listener = TcpListener::bind("127.0.0.1:10035").unwrap();

    for stream in listener.incoming()
    {
        let stream = stream.expect("Unable to accept");
        handle_client(stream);
    }
}
